package by.epam.textwork.comparator;

import by.epam.textwork.composite.Component;

import java.util.Comparator;

/**
 * Class overrides the Comparator's method to compare two objects
 * @see by.epam.textwork.composite.Component
 */
public class SizeSort implements Comparator<Component>{
    @Override
    public int compare(Component obj1, Component obj2) {
        if(obj1.size() < obj2.size()){
            return -1;
        }
        if(obj1.size() > obj2.size()) {
            return 1;
        }
        return 0;
    }
}
