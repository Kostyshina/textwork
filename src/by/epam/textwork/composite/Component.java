package by.epam.textwork.composite;

/**
 * Design pattern implementation
 */
public interface Component {
    /**
     * Adding component to structure
     * @param c Transmitted value to add
     */
    void add(Component c);
    void remove(Component c);

    /**
     * Taking structure (if it's finite element - its value)
     * @return The structure, assembled into string
     */
    String takeComponents();

    /**
     * Component size
     * @return Size, the number of elements without punctuation
     */
    int size();
}
