package by.epam.textwork.composite;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Design pattern implementation
 * {@inheritDoc}
 */
public class Composite implements Component {

    /** List of components, which may be composite or leaf */
    private ArrayList<Component> components = new ArrayList<>();

    public Composite(){}

    public Composite(Collection<Component> components){
        this.components.addAll(components);
    }

    @Override
    public void add(Component c) {
        components.add(c);
    }

    @Override
    public void remove(Component c) { components.remove(c); }

    @Override
    public String takeComponents() {
        String str = "";
        for(Component c : components){
            str += c.takeComponents();
        }
        return str;
    }

    public String takeComponents(String delimiter) {
        String str = "";
        for(Component c : components){
            if(!(c.size()==0)) {
                str += c.takeComponents().trim();
                str += delimiter;
            }
        }
        return str;
    }

    @Override
    public int size() {
        int i = 0;
        for(Component c : components){
            if(!(c.size() == 0)){
                i++;
            }
        }
        return i;
    }

    public ArrayList<Component> getComponents() {
        return components;
    }

    public void setComponents(ArrayList<Component> components) {
        this.components.clear();
        this.components.addAll(components);
    }
}
