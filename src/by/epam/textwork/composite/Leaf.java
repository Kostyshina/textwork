package by.epam.textwork.composite;

/**
 * Design pattern implementation. Class of finite element
 * {@inheritDoc}
 */
public class Leaf implements Component{
    private String leaf;

    @Override
    public void add(Component c) {}

    @Override
    public void remove(Component c) {}


    @Override
    public String takeComponents() {
        return leaf;
    }

    @Override
    public int size() {
        String symbols = "\r\n|\\p{Punct}";
        if(!leaf.matches(symbols)) {
            return leaf.length();
        }
        else{
            return 0;
        }
    }

    public void setLeaf(String leaf) {
        this.leaf = leaf;
    }
}
