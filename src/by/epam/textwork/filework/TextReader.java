package by.epam.textwork.filework;

import by.epam.textwork.exception.TechnicException;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Map;
import java.util.Scanner;
import java.util.TreeMap;

/**
 * Class to read input text and dividing it into paragraphs and listing
 */

public class TextReader {
    {
        listing = new TreeMap<>();
        paragraph = "";
    }

    public static final Logger LOG = Logger.getLogger(TextReader.class);
    private Map<Integer, String> listing;
    private String paragraph;

    public Map<Integer, String> getListing() {
        return listing;
    }

    public void setListing(Map<Integer, String> listing) {
        this.listing = listing;
    }

    public String getParagraph() {
        return paragraph;
    }

    public void setParagraph(String paragraph) {
        this.paragraph = paragraph;
    }

    /**
     * Primary method, which is working with file
     * @param fileName File with text
     * @param parser Listing start and end "flag"
     * @param delimiter Delimiters between parts (paragraphs and listing)
     * @throws TechnicException Null parameters
     */
    public void takeText(String fileName, String parser, String delimiter) throws TechnicException{
        try {
            if(fileName == null | parser == null | delimiter == null){
                throw new TechnicException("Wrong arguments");
            }
            int i = 0;
            boolean flag = true;
            String line;
            Scanner scanner = new Scanner(new File(fileName));
            /**
             * Fragmentation of the text
             */
            while(scanner.hasNextLine()){
                i++;
                line = scanner.nextLine();
                while(line.isEmpty()) {
                    line = scanner.nextLine();
                }
                if((line.matches(parser) && !flag) || (!line.matches(parser) && flag)){
                    if(!flag){
                        this.listing.put(i, line);
                    }
                    else{
                        paragraph += line + delimiter;
                    }
                    flag = true;
                }
                else{
                    flag = false;
                    this.listing.put(i, line);
                }
            }
        }
        catch(FileNotFoundException e){
            LOG.error(fileName + " not found");
        }
    }
}
