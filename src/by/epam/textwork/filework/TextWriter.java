package by.epam.textwork.filework;

import by.epam.textwork.composite.Composite;
import by.epam.textwork.exception.TechnicException;
import org.apache.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;

/**
 * Class to write structure to the file
 */
public class TextWriter {

    public static final Logger LOG = Logger.getLogger(TextReader.class);

    /**
     * Write only component of structure (sentences, words etc.)
     * @param fileName File to write
     * @param text Component of structure
     * @throws TechnicException Null parameters
     */
    public void textWrite(String fileName, Composite text)throws TechnicException{
        try {
            if(fileName == null | text == null){
                throw new TechnicException("Wrong arguments");
            }
            PrintWriter p = new PrintWriter(new File(fileName));
            p.println(text.takeComponents("\r\n"));
            p.close();
        }
        catch(FileNotFoundException e){
            LOG.error(fileName + "not found");
        }
    }

    /**
     * Write all structure (all text: paragraphs with listing)
     * @param fileName File to write
     * @param textWriter Object containing paragraphs and listing
     * @see by.epam.textwork.filework.TextReader
     * @throws TechnicException Null parameters
     */
    public void writeAll(String fileName, TextReader textWriter) throws TechnicException{
        try {
            if(fileName == null | textWriter == null){
                throw new TechnicException("Wrong arguments");
            }
            String[] str = textWriter.getParagraph().split("\r\n");
            PrintWriter p = new PrintWriter(new File(fileName));
            for(int j = 0, i = 1; j < str.length; j++, i++){
                if(textWriter.getListing().containsKey(i)){
                    p.println(textWriter.getListing().get(i));
                    j--;
                }
                else{
                    p.println(str[j]);
                }
            }
            p.close();
        }
        catch(FileNotFoundException e){
            LOG.error(fileName + "not found");
        }
    }
}
