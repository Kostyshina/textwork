package by.epam.textwork.parser;

import by.epam.textwork.composite.Component;
import by.epam.textwork.composite.Composite;
import by.epam.textwork.composite.Leaf;
import com.sun.istack.internal.NotNull;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class to parse apart
 */
public class Parser {

    /**
     * Picking up a component Composite
     * @param c Composite object
     * @param delimiter Delimiters for this text level
     * @return List of components
     */
    @NotNull
    public ArrayList<Component> takeComposite(Composite c, String delimiter){
        Pattern pattern = Pattern.compile(delimiter);
        Matcher matcher;
        ArrayList<Component> composites = new ArrayList<>();
        ArrayList<Component> parts = new ArrayList<>();
        Composite composite;
        for(Component el : c.getComponents()) {
            composite = new Composite();
            matcher = pattern.matcher(el.takeComponents());
            if (matcher.find()) {
                if(!parts.isEmpty()){
                    composite.setComponents(parts);
                    composites.add(composite);
                    parts.clear();
                }
                composites.add(el);
            }
            else {
                parts.add(el);
            }
        }
        if(!parts.isEmpty()){
            composite = new Composite();
            composite.setComponents(parts);
            composites.add(composite);
            parts.clear();
        }
        return composites;
    }

    /**
     * Assignment finite element of structure (symbol, word etc.)
     * @param string Text to parse
     * @param delimiter Delimiters for Leaf
     * @return List of leafs
     */
    @NotNull
    public ArrayList<Component> takeLeaf(String string, String delimiter){
        Pattern pattern = Pattern.compile(delimiter);
        Matcher matcher;
        ArrayList<Component> composites = new ArrayList<>();
        Leaf leaf;
        int ind = 0;
        ArrayList<String> symbol = new ArrayList<>();
        ArrayList<String> parts = new ArrayList<>();
        String[] tmp;
        matcher = pattern.matcher(string);
        while (matcher.find()) {
            symbol.add(matcher.group());
        }
        tmp = pattern.split(string);
        for(String t : tmp){
            if(!"".equals(t)) {
                parts.add(t);
            }
            if(symbol.get(ind).isEmpty()){
                symbol.remove(ind);
            }
            else{
                parts.add(symbol.get(ind));
                ind++;
            }
        }
        for (String p : parts) {
            leaf = new Leaf();
            leaf.setLeaf(p);
            composites.add(leaf);
        }
        return composites;
    }
}
