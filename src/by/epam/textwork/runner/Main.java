package by.epam.textwork.runner;

import by.epam.textwork.comparator.SizeSort;
import by.epam.textwork.exception.TechnicException;
import by.epam.textwork.filework.TextReader;
import by.epam.textwork.filework.TextWriter;
import by.epam.textwork.task.Config;
import by.epam.textwork.task.WordReplace;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

import java.util.Collections;

/**
 * @author Lisa Kostyshina
 * @version 5
 */
public class Main {
    static {
        PropertyConfigurator.configure("log4j.properties");
    }

    /** {@value} */
    public static final Logger LOG = Logger.getLogger(Main.class);

    public static void main(String[] args) throws TechnicException {
        String input = "files/inputText.txt";
        String task2 = "files/output1.txt";
        String task16 = "files/output2.txt";
        String listingParser = "^_.*$|^.*_$";
        String paragraphParser = "\r\n";
        String sentenceParser = "[\r\n\\.\\?!]";
        String wordParser = "[\r\n\\s\\-:.?!,;{}()<>]+";
        String symbolParser = "\r\n|";

        /** Parameters for
         * {@code WordReplace} */
        int length = 2;
        String replaceTo = "true JAVA";

        TextReader textReader = new TextReader();
        Config config = new Config();

        textReader.takeText(input, listingParser, paragraphParser);
        config.takeStructure(textReader.getParagraph(), paragraphParser, sentenceParser, wordParser, symbolParser);
        LOG.info("Composite is created");

        TextWriter textWriter = new TextWriter();
        Collections.sort(config.getSentences().getComponents(), new SizeSort());
        textWriter.textWrite(task2, config.getSentences());
        LOG.info("Task 1");

        WordReplace wordReplace = new WordReplace();
        config.setWords(wordReplace.replace(config.getWords(), length, replaceTo, symbolParser));
        config.takeStructure(textReader.getParagraph(), paragraphParser, sentenceParser);
        textReader.setParagraph(config.getParagraphs().takeComponents());
        textWriter.writeAll(task16, textReader);
        LOG.info("Task 2");
    }
}
