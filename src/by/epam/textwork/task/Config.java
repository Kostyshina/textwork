package by.epam.textwork.task;

import by.epam.textwork.composite.Composite;
import by.epam.textwork.exception.TechnicException;
import by.epam.textwork.parser.Parser;

/**
 * Configuration of the structure
 */
public class Config {

    private Composite paragraphs;
    private Composite sentences;
    private Composite words;
    private Composite symbols;

    public void setSymbols(Composite symbols) {
        this.symbols = symbols;
    }

    public void setWords(Composite words) {
        this.words = words;
    }

    public Composite getParagraphs() {
        return paragraphs;
    }

    public Composite getSentences() {
        return sentences;
    }

    public Composite getWords() {
        return words;
    }

    /**
     * Primary method for getting structure
     * @param paragraph Text to parse
     * @param parsers 1-4 parsers for all levels (symbol, word, sentence, paragraph)
     * @throws TechnicException No parsers
     */
    public void takeStructure(String paragraph, String ... parsers) throws TechnicException{
        Parser parser = new Parser();
        int length = parsers.length;

        switch(length){
            case 4 : { symbols = new Composite(parser.takeLeaf(paragraph, parsers[3]));
                length--;
            }
            case 3 : { words = new Composite(parser.takeComposite(symbols, parsers[2]));
                length--;
            }
            case 2 : { sentences = new Composite(parser.takeComposite(words, parsers[1]));
                length--;
            }
            case 1 : paragraphs = new Composite(parser.takeComposite(sentences, parsers[0]));
            default: {if(paragraphs == null){
                throw new TechnicException("Structure wasn't created");
            }}
        }
    }
}
