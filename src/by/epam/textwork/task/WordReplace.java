package by.epam.textwork.task;

import by.epam.textwork.composite.Composite;
import by.epam.textwork.parser.Parser;
import com.sun.istack.internal.NotNull;

/**
 * Task: replace all words predetermined length to given string
 */
public class WordReplace {
    private Composite symbols;

    @NotNull
    public Composite replace(Composite words, int length, String replaceTo, String parsers){
        Parser parser = new Parser();

        symbols = new Composite(parser.takeLeaf(replaceTo, parsers));

        words.getComponents().stream().forEach((c)->{
            if(c.size() == length){
                int ind = words.getComponents().indexOf(c);
                words.getComponents().set(ind,symbols);
            }
        });
        return words;
    }
}
